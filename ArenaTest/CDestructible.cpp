#include "CDestructible.h"

CDestructible::CDestructible(const char* pDeathName, float pMaxHealth)
	: deathName(pDeathName), maxHealth(pMaxHealth), health(pMaxHealth)
{

}

float CDestructible::takeDamage(Actor *owner, float amount)
{
	float realDamage = amount;
	health -= realDamage;
	
	if(health <= 0)
		die(owner);

	return realDamage;
}

float CDestructible::heal(Actor *owner, float amount)
{
	float realHeal = amount;
	health += realHeal;
	if(health > maxHealth)
		health = maxHealth;

	return realHeal;
}

void CDestructible::die(Actor *owner)
{
	owner->ch = '%';
	owner->col = TCODColor::darkRed;
	owner->isEtheral = true;
	owner->name = deathName;
	engine.moveActorToNecro(owner);
}