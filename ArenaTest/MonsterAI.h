#pragma once

#include "main.h"

class MonsterAI :
	public AI
{
public:
	MonsterAI() : morale(1)
	{
		isFleeing = false;
	}

	MonsterAI(int morale) : morale(morale)
	{
		isFleeing = false;
	}

	void update(Actor *owner);
	void damageMorale(Actor* owner, int amount);

protected:
	static const int TRACKING_TURNS = 3;
	static const int FLEEING_TURNS = 3;

	int trackCount;
	int morale;
	bool isFleeing;
	int trackFleeingCount;

	void moveOrAttack(Actor *owner, int targetx, int targety, bool flee);
};

