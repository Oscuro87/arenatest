#pragma once

#include "main.h"

enum TerrainType
{
	GRASS,
	WALL,
};

struct Tile
{
	bool explored;
	TCODColor tileColorUnlit;
	TCODColor tileColorLit;
	char tileGraphic;

	Tile(char pTileGraphic, TCODColor pTileColorUnlit, TCODColor pTileColorLit) : tileGraphic(pTileGraphic), tileColorUnlit(pTileColorUnlit), tileColorLit(pTileColorLit), explored(false) {}
	Tile() : tileGraphic(' '), tileColorUnlit(TCODColor::darkGreen), tileColorLit(TCODColor::green), explored(false) {}
};

class Map
{
public:
	int width;
	int height;
	Tile* tiles;
	TCODMap *map;
	ActorSpawner* spawner;

	Map(int pW, int pH);
	~Map();
	
	bool isWall(int x, int y) const;
	void render() const;
	bool isInFOV(int x, int y) const;
	bool isInFOV(Actor* target) const;
	bool isInEnemyFOV(Actor* enemy) const;
	bool isExplored(int x, int y) const;
	void computeFOV();
	bool canWalk(int x, int y) const;
	
	void addMonster();
	void setTerrain(int x, int y, TerrainType type);
};