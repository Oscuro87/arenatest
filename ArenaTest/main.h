#include <libtcod\libtcod.hpp>
#include <string>

#include "Logger.h"
class Actor;
class CDestructible;
class AI;
class Attacker;
class ActorSpawner;
class GUI;
#include "CDestructible.h"
#include "Attacker.h"
#include "AI.h"
#include "Actor.h"
#include "Map.h"
#include "Engine.h"
#include "ActorSpawner.h"
#include "GUI.h"