#pragma once

#include "main.h"

class CDestructible
{
public:
	float maxHealth;
	float health;
	const char* deathName;

	CDestructible(const char* pDeathName, float pMaxHealth);

	inline bool isDead() { return health <= 0.f; }
	virtual float takeDamage(Actor *owner, float amount);
	float heal(Actor *owner, float amount);
	virtual void die(Actor *owner);
};