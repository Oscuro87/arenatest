#pragma once

#include "main.h"

class GUI
{
public:
	GUI();
	~GUI();
	void render();

protected:
	static const int PANEL_HEIGHT = 7;
	static const int BAR_WIDTH = 20;

	TCODConsole* con;

	void renderBar(int x, int y, int width, const char *name,
		float value, float maxValue, const TCODColor &barColor,
		const TCODColor &backColor);
};

