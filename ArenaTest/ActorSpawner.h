#pragma once

#include "main.h"

enum ActorType
{
	PLAYER,
	GOBLIN,
};

class ActorSpawner
{
public:
	ActorSpawner();
	~ActorSpawner();

	// Spawns a new Actor at set location, returns false if position cannot be used as spawn point (occupied, etc...).
	bool spawnActor(int x, int y, ActorType type);
	// Spawns a new Actor at a random, free location on the map.
	void spawnActor(ActorType type);
};