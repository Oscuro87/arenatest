#pragma once

#include "main.h"

class Map;

enum GameState
{
	STARTUP,
	MAPEDITOR,
	INMENU,
	MOUSELOOK,
	IDLE,
	NEWTURN,
	VICTORY,
	DEFEAT,
};

class Engine
{
private:
	bool isRunning;

	void loop();

public:
	int screenW, screenH;
	TCOD_key_t lastKeyboardEvent;
	TCOD_mouse_t lastMouseEvent;

	Map* map;
	GUI* gui;

	TCODList<Actor *> necro;
	TCODList<Actor *> actors;
	Actor * player;
	GameState state;

	Logger* log;
	
	bool computeFOV;

	Engine();
	Engine(int width, int height);
	~Engine();

	void start();
	void stop();

	void moveActorToNecro(Actor* actor);

	void render();
	void update();
};

extern Engine engine;