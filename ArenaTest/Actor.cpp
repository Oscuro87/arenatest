#pragma once

#include <stdio.h>

#include "Actor.h"
#include "Engine.h"

Actor::Actor(int pX, int pY, const char* pName, bool pIsEtheral,
				  int pCh, const TCODColor pColor, int FOVRange)
				  : x(pX), y(pY), isEtheral(pIsEtheral), name(pName),
				  ch(pCh), col(pColor), FOVRange(FOVRange), destructible(0), attacker(0),
				  ai(0)
{
}

Actor::~Actor()
{
	if(destructible)
		delete destructible;

	if(attacker)
		delete attacker;

	if(ai)
		delete ai;
}

void Actor::render()
{
	TCODConsole::root->setChar(x, y, ch);
	TCODConsole::root->setCharForeground(x, y, col);
}

void Actor::update()
{
	if(ai) ai->update(this);
}