#pragma once

#include "main.h"

class Actor
{
public:
	int ch;
	int x,y;
	const char* name;
	TCODColor col;

	bool isEtheral;
	int FOVRange;
	
	CDestructible* destructible;
	Attacker* attacker;
	AI* ai;


	Actor(int pX, int pY, const char* pName, bool pIsEtheral,
		int pCh, const TCODColor pColor, int FOVRange);

	~Actor();

	void render();
	void update();

	bool moveOrAttack(int x, int y);
};