#include "MapImportExport.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

MapImportExport::MapImportExport()
{
}

MapImportExport::~MapImportExport()
{
}

bool MapImportExport::importMap(const char* filePath)
{
	file = ifstream(filePath, ios::in);
	if(file.is_open())
	{
		calculateMapSize();
		theMap = new Map(mapX, mapY);
		importTiles();
	}
	else
	{
		printf("Unable to load map file: %s!", filePath);
		engine.stop();
		return false;
	}
	return true;
}

void MapImportExport::exportMap()
{
	theMap = engine.map;
	// PROCESS
	theMap = 0;
}

void MapImportExport::calculateMapSize()
{
	string line;
	getline(file, line);
	file.seekg(ios::beg);
	mapX = line.length();

	while(getline(file, line))
		++mapY;
}

void MapImportExport::importTiles()
{
	// Read tiles from file
}