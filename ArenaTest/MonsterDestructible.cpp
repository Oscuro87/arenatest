#include "MonsterDestructible.h"

MonsterDestructible::MonsterDestructible(const char* pDeathName, float pMaxHealth)
	: CDestructible(pDeathName, pMaxHealth)
{
}

float MonsterDestructible::takeDamage(Actor *owner, float amount)
{
	static_cast<MonsterAI*>(owner->ai)->damageMorale(owner, 1);
	return CDestructible::takeDamage(owner, amount);
}

void MonsterDestructible::die(Actor *owner)
{
	printf("The %s dies.\n", owner->name);
	CDestructible::die(owner);
}