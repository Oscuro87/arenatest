#include "PlayerAI.h"

void PlayerAI::update(Actor *owner)
{
	if(owner->destructible && owner->destructible->isDead())
		return;

	int dx = 0, dy = 0;

	switch(engine.lastKeyboardEvent.vk)
	{
	case TCODK_KP9:
		++dx;
		--dy;
		break;
	case TCODK_KP8:
		dy = -1;
		break;
	case TCODK_KP7:
		--dy;
		--dx;
		break;
	case TCODK_KP6:
		dx = 1;
		break;
	case TCODK_KP5:
		engine.state = NEWTURN;
		// Wait a turn!
		break;
	case TCODK_KP4:
		dx = -1;
		break;
	case TCODK_KP3:
		++dx;
		++dy;
		break;
	case TCODK_KP2:
		dy = 1;
		break;
	case TCODK_KP1:
		--dx;
		++dy;
		break;
	
	case TCODK_ESCAPE:
		engine.stop();
		break;

	default:break;
	}

	if(dx != 0 || dy != 0)
	{
		engine.state = NEWTURN;
		if(moveOrAttack(owner, owner->x+dx, owner->y+dy))
			engine.map->computeFOV();
	}
}

bool PlayerAI::moveOrAttack(Actor *owner, int targetx, int targety)
{
	if(targetx < 0 || targetx >= engine.map->width
		|| targety < 0 || targety >= engine.map->height)
		return false;

	if(!owner->isEtheral)
		if(engine.map->isWall(targetx, targety))
			return false;

	for(Actor** it = engine.actors.begin(); 
			it != engine.actors.end(); ++it)
	{
		Actor* act = (*it);
		if(act->destructible && !act->destructible->isDead()
			&& act->x == targetx && act->y == targety)
		{
			owner->attacker->attack(owner, act);
			return false;
		}
	}

	owner->x = targetx;
	owner->y = targety;
	return true;
}