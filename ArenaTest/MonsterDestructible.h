#pragma once
#include "main.h"
#include "MonsterAI.h"

class MonsterDestructible :
	public CDestructible
{
public:
	MonsterDestructible(const char* pDeathName, float pMaxHealth);
	void die(Actor *owner);
	virtual float takeDamage(Actor *owner, float amount);
};

