#include <stdio.h>

#include "Engine.h"

Engine engine;

int main()
{
	engine.start();
	return 0;
}