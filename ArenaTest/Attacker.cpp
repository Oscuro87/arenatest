#include "Attacker.h"


Attacker::Attacker(float pAttackPower)
	: attackPower(pAttackPower)
{

}

void Attacker::attack(Actor *owner, Actor *target)
{
	float damage = owner->attacker->attackPower;
	printf("%s attacks %s and deals %f damage.\n", owner->name, target->name, damage);
	target->destructible->takeDamage(target, attackPower);
}