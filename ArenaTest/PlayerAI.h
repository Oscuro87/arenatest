#pragma once

#include "main.h"

class PlayerAI : public AI
{
public:
	void update(Actor *owner);

protected:
	bool moveOrAttack(Actor *owner, int targetx, int targety);
};