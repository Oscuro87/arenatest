#include "ActorSpawner.h"

#include "PlayerDestructible.h"
#include "MonsterDestructible.h"
#include "PlayerAI.h"
#include "MonsterAI.h"

ActorSpawner::ActorSpawner(void)
{
}

ActorSpawner::~ActorSpawner()
{

}

bool ActorSpawner::spawnActor(int x, int y, ActorType type)
{
	Map *theMap = engine.map;
	Actor *newAct = 0;
	if(!theMap->canWalk(x,y)) return false;
	
	switch(type)
	{
	case PLAYER:
		newAct = new Actor(x,y,"Player",false,'@',TCODColor::yellow, 10);
		newAct->destructible = new PlayerDestructible("Your corpse... !!!", 100.f);
		newAct->attacker = new Attacker(5.f);
		newAct->ai = new PlayerAI();
		engine.player = newAct;
		engine.actors.push(newAct);
		break;
	
	case GOBLIN:
		newAct = new Actor(x,y,"Goblin",false,'g',TCODColor::darkerGreen, 5);
		newAct->destructible = new MonsterDestructible("A goblin's corpse", 15.f);
		newAct->attacker = new Attacker(2.f);
		newAct->ai = new MonsterAI();
		engine.actors.push(newAct);
		break;
	
	default:
		return false;
	}

	return true;
}

void ActorSpawner::spawnActor(ActorType type)
{
	Map *theMap = engine.map;
	int rx = 0, ry = 0;

	do
	{
		rx = TCODRandom::getInstance()->getInt(0, theMap->width);
		ry = TCODRandom::getInstance()->getInt(0, theMap->height);
	} while(!theMap->canWalk(rx,ry));
	spawnActor(rx,ry,type);
}