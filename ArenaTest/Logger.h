#pragma once

#include <iostream>
#include <fstream>

class Logger
{
private:
	const char* logName;
	std::ofstream stream;

public:
	Logger();
	~Logger();

	void addLogLine(const char* data);
	void addLogLine(std::string data);
};