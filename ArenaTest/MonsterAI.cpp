#include "MonsterAI.h"

#include <math.h>

void MonsterAI::update(Actor *owner)
{
	if(owner->destructible && owner->destructible->isDead()) return;

	if(isFleeing)
	{
		++trackFleeingCount;
		if(trackFleeingCount == FLEEING_TURNS)
			isFleeing = 0;
		moveOrAttack(owner, engine.player->x, engine.player->y, isFleeing);
	}
	else
	{
		if(engine.map->isInEnemyFOV(owner))
			trackCount = TRACKING_TURNS;
		else
			trackCount--;
	
		if(trackCount > 0)	
			moveOrAttack(owner, engine.player->x, engine.player->y, isFleeing);
	}
}

void MonsterAI::damageMorale(Actor* owner, int amount)
{
	morale -= amount;
	if(morale <= 0)
	{
		printf("The %s starts fleeing!\n", owner->name);
		trackFleeingCount = 0;
		isFleeing = true;
	}
}

void MonsterAI::moveOrAttack(Actor *owner, int targetx, int targety, bool flee)
{
	int dx, dy;
	if(isFleeing)
	{
		dx = -targetx - owner->x;
		dy = -targety - owner->y;
	}
	else
	{
		dx = targetx - owner->x;
		dy = targety - owner->y;
	}
	int stepdx = (dx > 0 ? 1:-1);
	int stepdy = (dy > 0 ? 1:-1);
	float distance = sqrtf(dx*dx+dy*dy);

	if(distance >= 2)
	{
		dx = (int)(dx/distance < 0.0 ? ceil(dx/distance - 0.5) : floor(dx/distance + 0.5));
		dy = (int)(dy/distance < 0.0 ? ceil(dy/distance - 0.5) : floor(dy/distance + 0.5));

		if(engine.map->canWalk(owner->x+dx, owner->y+dy))
		{
			owner->x += dx;
			owner->y += dy;
		}
		else if(engine.map->canWalk(owner->x+stepdx, owner->y))
			owner->x += stepdx;
		else if(engine.map->canWalk(owner->x, owner->y+stepdy))
			owner->y += stepdy;
	}
	else if(owner->attacker)
			owner->attacker->attack(owner, engine.player);
}