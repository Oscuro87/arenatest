#pragma once
#include "main.h"

class AI
{
public:
	virtual void update(Actor *owner) = 0;
};