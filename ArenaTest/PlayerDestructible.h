#pragma once

#include "CDestructible.h"

class PlayerDestructible :
	public CDestructible
{
public:
	PlayerDestructible(const char* pDeathName, float pMaxHealth);
	void die(Actor *owner);
};

