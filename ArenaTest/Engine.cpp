#include "Engine.h"

#include "PlayerDestructible.h"
#include "PlayerAI.h"

Engine::Engine() : screenW(100), screenH(75), computeFOV(true), state(STARTUP)
{
	TCODConsole::initRoot(screenW, screenH, "Test", false);

	log = new Logger();

	//TODO show main menu GUI instead of jumping directly in the game

	map = new Map(30, 30);
	gui = new GUI();

	map->addMonster();
	map->spawner->spawnActor(ActorType::PLAYER);
}

Engine::Engine(int width, int height) : screenW(width), screenH(height)
{
	TCODConsole::initRoot(screenW, screenH, "Test", false);

	log = new Logger();

	//TODO show main menu GUI instead of jumping directly in the game

	map = new Map(20, 14);
	
	map->addMonster();
	map->spawner->spawnActor(ActorType::PLAYER);
}

Engine::~Engine()
{
	actors.clearAndDelete();
	player = 0;
	
	if(map != 0)
		delete map;

	if(log != 0)
		delete log;

	if(gui)
		delete gui;
}

void Engine::loop()
{
	while(!TCODConsole::isWindowClosed() && isRunning)
	{
		TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &lastKeyboardEvent, NULL);
		TCODSystem::checkForEvent(TCOD_EVENT_MOUSE_PRESS, NULL, &lastMouseEvent);
		TCODConsole::root->clear();
		update();
		render();
		TCODConsole::root->flush();
	}
}

void Engine::start()
{
	isRunning = true;
	loop();
}

void Engine::stop()
{
	isRunning = false;
}

void Engine::update()
{
	player->update();

	// Update monsters
	if(state == NEWTURN)
	{
		for(Actor** it = actors.begin(); it != actors.end(); ++it)
		{
			if((*it) != player)
				(*it)->update();
		}
	}

	if(state == STARTUP)
		map->computeFOV();
	else if(state == DEFEAT)
	{
		log->addLogLine("Player has been defeated...");
	}
	state = IDLE;
}

void Engine::render()
{
	map->render();

	for(Actor** iterator = necro.begin(); iterator != necro.end();
		++ iterator)
	{
		Actor *act = (*iterator);
		if(map->isInFOV(act->x, act->y))
			(*iterator)->render();
	}

	for(Actor** iterator = actors.begin(); iterator != actors.end();
		++ iterator)
	{
		Actor *act = (*iterator);
		if(map->isInFOV(act->x, act->y))
			(*iterator)->render();
	}

	gui->render();
}

void Engine::moveActorToNecro(Actor* actor)
{
	actors.remove(actor);
	necro.insertBefore(actor, 0);
}