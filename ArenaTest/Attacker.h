#pragma once

#include "main.h"

class Attacker
{
public:
	float attackPower;

	Attacker(float pAttackPower);
	void attack(Actor *owner, Actor *target);
};

