#include "Map.h"

#include "MonsterDestructible.h"

Map::Map(int pW, int pH) : width(pW), height(pH)
{
	tiles = new Tile[width*height];
	map = new TCODMap(width, height);
	spawner = new ActorSpawner();

	for(int x = 0; x < width ; ++x)
		for(int y = 0 ; y < height ; ++y)
			setTerrain(x, y, GRASS);
}

Map::~Map()
{
	delete [] tiles;
	if(map)
		delete map;
	if(spawner)
		delete spawner;
}

bool Map::isWall(int x, int y) const
{
	return !map->isWalkable(x,y);
}

void Map::render() const
{
	for(int x = 0; x < width ; ++x)
	{
		for(int y = 0; y < height ; ++y)
		{
			if(isInFOV(x,y))
				TCODConsole::root->setCharBackground(x,y, tiles[x+y*width].tileColorLit);
			else if(isExplored(x,y))
				TCODConsole::root->setCharBackground(x,y, tiles[x+y*width].tileColorUnlit);
		}
	}
}

bool Map::isInFOV(int x, int y) const
{
	if(map->isInFov(x,y))
	{
		tiles[x+y*width].explored = true;
		return true;
	}
	return false;
}

bool Map::isInEnemyFOV(Actor* enemy) const
{
	float distance = sqrtf( powf(engine.player->x - enemy->x, 2) + powf(engine.player->y - enemy->y, 2) );
	if(distance <= enemy->FOVRange)
		return true;
	return false;
}

bool Map::isInFOV(Actor* target) const
{
	return Map::isInFOV(target->x, target->y);
}

bool Map::isExplored(int x, int y) const
{
	return tiles[x+y*width].explored;
}

void Map::computeFOV()
{
	map->computeFov(engine.player->x, engine.player->y, engine.player->FOVRange, true, TCOD_fov_algorithm_t::FOV_SHADOW);
}

bool Map::canWalk(int x, int y) const
{
	if(x < 0 || x > width - 1 || y < 0 || y > height - 1)
		return false;

	if(isWall(x,y))
		return false;

	for(Actor** it = engine.actors.begin(); it != engine.actors.end();
		++it)
	{
		Actor *act = (*it);
		if(!act->isEtheral && act->x == x && act->y == y)
			return false;
	}
	return true;
}

void Map::addMonster()
{
	spawner->spawnActor(ActorType::GOBLIN);
}

void Map::setTerrain(int x, int y, TerrainType type)
{
	switch(type)
	{
	case GRASS:
		tiles[x+y*width] = Tile(' ', TCODColor::darkGreen, TCODColor::green);
		map->setProperties(x,y,true,true);
		break;

	case WALL:
		tiles[x+y*width] = Tile(' ', TCODColor::darkGrey, TCODColor::grey);
		map->setProperties(x,y,false,false);
		break;

	default:
		tiles[x+y*width] = Tile('X', TCODColor::black, TCODColor::black);
		map->setProperties(x,y,false,false);
		break;
	}
}