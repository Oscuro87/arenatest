#include "main.h"

#include <fstream>

class MapImportExport
{
private:
	int mapX, mapY;
	Map *theMap;
	std::ifstream file;

	// For import
	void calculateMapSize();
	void importTiles();

	// For export

public:
	MapImportExport();
	~MapImportExport();

	bool importMap(const char* filePath);
	void exportMap();
};