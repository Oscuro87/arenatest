#include "GUI.h"

GUI::GUI()
{
	con = new TCODConsole(engine.screenW, PANEL_HEIGHT);
}

GUI::~GUI()
{
	if(con)
		delete con;
}

void GUI::render()
{
	con->setDefaultBackground(TCODColor::black);
	con->setDefaultForeground(TCODColor::white);
	con->clear();

	renderBar(1,1,BAR_WIDTH,"HP",engine.player->destructible->health,engine.player->destructible->maxHealth,TCODColor::lightRed,TCODColor::darkerRed);

	TCODConsole::blit(con,0,0,engine.screenW,PANEL_HEIGHT,TCODConsole::root,0,engine.screenH-PANEL_HEIGHT);
}

void GUI::renderBar(int x, int y, int width, const char *name,
		float value, float maxValue, const TCODColor &barColor,
		const TCODColor &backColor)
{
	con->setDefaultBackground(backColor);
	con->rect(x,y,width,1,false,TCOD_BKGND_SET);

	int barW = (int)(value/maxValue*width);
	if(barW > 0)
	{
		con->setDefaultBackground(barColor);
		con->rect(x,y,barW,1,false,TCOD_BKGND_SET);
	}

	con->setDefaultForeground(TCODColor::white);
	con->printEx(x+width/2,y,TCOD_BKGND_NONE,TCOD_CENTER,"%s : %g/%g", name, value, maxValue);
}