#include "PlayerDestructible.h"


PlayerDestructible::PlayerDestructible(const char* pDeathName, 
									   float pMaxHealth)
	: CDestructible(pDeathName, pMaxHealth)
{
}

void PlayerDestructible::die(Actor *owner)
{
	printf("You are dead.\n");
	CDestructible::die(owner);
	engine.state = DEFEAT;
}