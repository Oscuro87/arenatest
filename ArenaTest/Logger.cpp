#include "Logger.h"

using namespace std;

Logger::Logger() : logName("Log.txt")
{
	stream.open(logName, std::ios::out | std::ios::trunc);
}

Logger::~Logger()
{
	stream.close();
}

void Logger::addLogLine(const char* data)
{
	stream << data;
}

void Logger::addLogLine(string data)
{
	stream << data.c_str();
}